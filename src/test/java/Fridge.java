package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();


	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	public int nItemsInFridge() {
		return items.size();
	}

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	public int totalSize() {
		return 20;
	}

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	public boolean placeIn(FridgeItem item) {
		if(nItemsInFridge() < totalSize()){
			items.add(item);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	public void takeOut(FridgeItem item) {
		if(!items.contains(item)) {
			throw new NoSuchElementException();
		}
		else {
			items.remove(item);
		}
	}

	/**
	 * Remove all items from the fridge
	 */
	public void emptyFridge() {
		items.clear();
	}

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();

		for (int k = 0; k < nItemsInFridge(); k++){
			if(items.get(k).hasExpired()){
				expired.add(items.get(k));
				takeOut(items.get(k));
				k -=1;
			}
		}
		return expired;
	}
}
