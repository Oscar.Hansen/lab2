package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public interface IFridge{
	ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();


	/**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	public default int nItemsInFridge() {
		return items.size();
	}

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	public default int totalSize() {
		return 20;
	}

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	public default boolean placeIn(FridgeItem item) {
		if(nItemsInFridge() < totalSize()){
			items.add(item);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	public default void takeOut(FridgeItem item) {
		if(!items.contains(item)) {
			throw new NoSuchElementException();
		}
		else {
			items.remove(item);
		}
	}

	/**
	 * Remove all items from the fridge
	 */
	public default void emptyFridge() {
		items.clear();
	}

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	public default List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();

		for (int i = 0; i < nItemsInFridge(); i++){
			if(items.get(i).hasExpired()){
				expired.add(items.get(i));
				takeOut(items.get(i));
				i -=1;
			}
		}
		return expired;
	}

}
